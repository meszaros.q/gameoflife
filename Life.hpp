#include <unordered_map>
#include <array>
#include <vector>
#include <cstdint>

namespace life {

class Life {
public:
    struct Coord
    {
        int32_t x = 0, y = 0;
        bool operator==(const Coord &c) const { return x == c.x && y == c.y; }
    };

private:

    static constexpr int min_to_live = 2, max_to_live = 3;
    
    struct HashFn {
        size_t operator()(const Coord &c) const noexcept
        {
            uint64_t h = c.x;
            h = h << 32;
            h += c.y;

            return std::hash<uint64_t>{}(h);
        }
    };

    using Scene = std::unordered_map<Coord, bool, HashFn>;

    Scene m_scene;
    Scene m_changed;

    static std::array<Coord, 8> neighbors(Coord c)
    {
        std::array<Coord, 8> ret = {{
            {c.x - 1, c.y - 1},
            {c.x, c.y - 1},
            {c.x + 1, c.y - 1},
            {c.x - 1, c.y},
            {c.x + 1, c.y},
            {c.x - 1, c.y + 1},
            {c.x, c.y + 1},
            {c.x + 1, c.y + 1}
        }};

        return ret;
    }

    static bool evaluate(bool v, int alive_sum)
    {
        return (v && alive_sum >= min_to_live && alive_sum <= max_to_live) || 
               (!v && (alive_sum == max_to_live));
    }

    bool update_cell(const Coord &c)
    {
        int alive_sum = 0;
        auto neighs = neighbors(c);

        for (const Coord &n : neighs) {
            auto it = m_scene.find(n);
            alive_sum += (it != m_scene.end()) && it->second;
        }

        bool v = m_scene[c];
        bool vv = v;
        if (!v && !alive_sum)
            m_scene.erase(c);
        else
            v = evaluate(v, alive_sum);

        return v != vv;
    }

public:
    template<class It> Life(It from, It to)
    {
        m_changed.reserve(std::distance(from, to));
        for (auto it = from; it != to; ++it) {
            m_scene[*it] = true;
            m_changed = m_scene;
        }
    }

    Life(const std::vector<Coord> &init) : Life(init.begin(), init.end()) {}

    template<class Fn> void for_each_changed(Fn fn) const
    {
        for (const auto &cell : m_changed)
            fn(cell.first);
    }

    template<class Fn> void for_each_live(Fn fn) const
    {
        for (const auto &cell : m_scene)
            fn(cell.first);
    }

    bool get_value(const Coord &c) const
    {
        auto it = m_scene.find(c);
        return (it != m_scene.end()) && it->second;
    }

    size_t live_count() const { return m_scene.size(); }

    void run_step()
    {
        m_changed.clear();

        // Collect the cells that need to be visited into m_changed
        for (const auto &cell : m_scene) {
            if (cell.second) {
                m_changed[cell.first] = false;

                auto neighs = neighbors(cell.first);
                for (const Coord &n : neighs)
                    m_changed[n] = false;
            }
        }

        // Update all cell that can possibly change
        for (auto &to_change : m_changed)
            to_change.second = update_cell(to_change.first);

        // Remember only the true changes and perform the change on the scene
        for (auto it = m_changed.begin(); it != m_changed.end(); ) {
            const auto &[key, change] = *it;
            if (!change)
                it = m_changed.erase(it);
            else {
                bool & v = m_scene[key];
                v = !v;
                ++it;
            }
        }

        // Keep only the ON cells in scene
        for (auto it = m_scene.begin(); it != m_scene.end(); )
            if (!it->second)
                it = m_scene.erase(it);
            else
                ++it;
    }
};

} // namespace life