#include <string>
#include <iostream>

#include "Life.hpp"

constexpr life::Life::Coord glider[] = {
    {0, 0}, {1, 1}, {-1, 2}, {0, 2}, {1, 2}
};

constexpr life::Life::Coord explode[] = {
    {0, 0}, {2, 0}, {2, 1}, {4, 2}, {4, 3}, {4, 4}, {6, 3}, {6, 4}, {6, 5}, {7, 4}
};

int main(const int argc, const char *argv[])
{
    int max_iter = -1;

    if (argc >= 2) max_iter = std::stoi(argv[1]);

    life::Life life(std::begin(explode), std::end(explode));

    int i = 0;
    while(life.live_count() > 0 && i++ != max_iter) {
        life.run_step();
        // std::cout << "Live cells: " << life.live_count() << std::endl;
    }

    return 0;
}